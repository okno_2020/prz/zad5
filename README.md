# Zadanie 5 – z plikami (0.5 pkt)

1. Z pliku o nazwie podanej przez użytkownika wczytać wierszami dane rzeczywiste
(mogą być ujemne, mogą być bez kropki) do tablicy A\[n\]\[n\] (n - stała). Jeśli danych w
pliku będzie za mało, wolne miejsca w tablicy A powinny być wypełnione wartością
stałą x.
2. Wydrukować tablicę wierszami ze stałą liczbą p miejsc po kropce i stałą
szerokością d kolumn.
3. Znaleźć największą liczbę na głównej przekątnej (lub pierwszą napotkaną z kilku
największych) i wpisać ją do całego wiersza, którym znajduje się ta liczba.
4. Znaleźć najmniejszą liczbę pod główną przekątną (lub pierwszą napotkaną z kilku
najmniejszych) i wpisać ją do całej kolumny, w której znajduje się ta liczba.
5. Ponownie wydrukować tablicę (w formacie jak w p. 2).
6. Na koniec całą tablicę zapisać wierszami (w formacie jak w p. 2) do pliku o nazwie
"nowa.txt"

Wskazówki do wczytywania tablicy (p. 1):

1. należy sygnalizować brak pliku z danymi o podanej nazwie.
2. najpierw wypełniamy stałą x całą tablicę, a potem z pliku wczytujemy wierszami tablicę
analogicznie jak z klawiatury – wczyta się co najwyżej tyle danych, ile jest w pliku (nie
trzeba sprawdzać eof).
