using namespace std;
#include <iostream>
#include <fstream>
#include <iomanip>

int main() {
    const int n = 4;    // wielkość tablicy
    const int d = 8;    // szerokość kolumny
    const int p = 3;    // dokładność wyświetlania
    const double x = 0; // liczba uzupełniająca puste pola w tablicy
    double maxVal, minVal; // najmniejsza i największa wartość
    int indexMaxVal = -1; // indexy min wartości
    int indexMinVal = -1; //index max wartości
    double A[n][n];


//    string filename = "data.txt";
    string filename;
    cout << "Podja nazwe pliku" << endl;
    cin >> filename;
    ifstream file(filename.c_str());

    if (!file.good()){
        cout << "plik nie istnieje" << endl;
        return 1;
    }


    for (int i = 0; i < n; ++i) {
        for (int j = 0; j <n; ++j) {
            //uzupełnij
            A[i][j] = x;
            // pobierz dane z pliku
            file >> A[i][j];

            // zresetuj wartości min i max;
            if (i == 0 && j == 0) {maxVal = A[0][0];}
            if (i == 1 & j == 0) {minVal = A[1][0];}

            //Wyznacz największą liczbę na głównej przekątnej
            if(i==j && maxVal < A[i][i]){
                maxVal = A[i][i];
                if(indexMaxVal == -1) {
                    indexMaxVal = i;
                }
            }
            //wyznacz najmniejszą liczbę pod główną przekątną
            if (i > j && minVal > A[i][j]) {
                minVal = A[i][j];
                if(indexMinVal == -1) {
                    indexMinVal = j;
                }
            }
        }
    }

    //zamkinj plik
    file.close();

    //wydrukuj
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j <n; ++j) {
            cout << fixed << setw(d) << setprecision(p) << A[i][j] << '\t';
        }
        cout << endl;
    }
    cout << endl;

    //uzupełnij wiersz maxVal
    for (int i = 0; i < n; ++i) {
        A[indexMaxVal][i] = maxVal;
        A[i][indexMinVal] = minVal;
    }

    //wydrukuj
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j <n; ++j) {
            cout << fixed << setw(d) << setprecision(p) << A[i][j] << '\t';
        }
        cout << endl;
    }
    cout << endl;

    string newFilename = "nowa.txt";
    ofstream newFile(newFilename.c_str());

    //wydrukuj
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j <n; ++j) {
            newFile << fixed << setw(d) << setprecision(p) << A[i][j] << '\t';
        }
        newFile << endl;
    }

    newFile.close();

    return 0;
}
